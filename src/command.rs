use crate::ErrorKind;
use std::str::FromStr;

/// Supported commands.
pub enum Command {
    /// A default command "s".
    S,
    /// An empty command, same as `Command::S`.
    Empty,
}

impl FromStr for Command {
    type Err = ErrorKind;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        match s {
            "" => Ok(Command::Empty),
            "s" => Ok(Command::S),
            cmd => Err(ErrorKind::UnknownCommand(cmd.into())),
        }
    }
}
